<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/util/Conexao.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of relogioDAO
 *
 * @author root
 */
class relogioDAO {

    public function inserir($relogio){
        try {
            $sql = 'insert into relogios (nome, codigo, preco, end_img)
         values (:nome, :codigo, :preco, :end_img)';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':nome', $relogio['nome']);
            $p_sql->bindValue(':codigo', $relogio['codigo']);
            $p_sql->bindValue(':preco', $relogio['preco']);
            $p_sql->bindValue(':end_img', $relogio['end_img']);
            $p_sql->execute();
            return true;
        } catch (Exception $e) {
            print_r($e);
        }

    }
    
    public function alterar($relogio, $id){
        print_r($relogio);
        print_r($id);
        try{
            $sql = 'update relogios set nome=?, codigo=?, preco=?, end_img=? where id=?';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(1 , $relogio['nome']);
            $p_sql->bindValue(2 , $relogio['codigo']);
            $p_sql->bindValue(3 , $relogio['preco']);
            $p_sql->bindValue(4 , $relogio['end_img']);
            $p_sql->bindValue(5 , $id);
            $p_sql->execute();
            return true;
          } catch (Exception $ex) {
              print_r($ex);
          }
            
        
    }
    
    public function buscar(){
        try {
            $sql = 'select * from relogios';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            print_r($e);
        }

    }
    
    public function buscarRegistro($id){
        try {
            $sql = 'select * from relogios where id=?';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(1,$id);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print_r($ex);   
        }
    }
    
    public function buscarMasculino(){
        try {
            $sql = 'select * from relogios where codigo like "m%"';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print_r($ex);   
        }
    }
      public function buscarFeminino(){
        try {
            $sql = 'select * from relogios where codigo like "f%"';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print_r($ex);   
        }
    }
    
    public function excluir($id){
        try {
            $sql = 'delete from relogios where id=?';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(1,$id);
            $p_sql->execute();
            return true;
        } catch (Exception $ex) {
            print_r($ex);
        }
    }
    
}


?>